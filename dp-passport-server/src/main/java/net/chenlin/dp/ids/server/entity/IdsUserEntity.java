package net.chenlin.dp.ids.server.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户model
 * @author zcl<yczclcn@163.com>
 */
public class IdsUserEntity implements Serializable {

    private static final long serialVersionUID = -7337047097412958927L;

    /**
     * 用户id
     */
    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * mds salt
     */
    private String salt;

    /**
     * 密码
     */
    private String password;

    /**
     * 用户状态，1：正常，0：禁用
     */
    private Integer status;

    /**
     * 上次登录时间
     */
    private Date gmtLastLogin;

    /**
     * constructor
     */
    public IdsUserEntity() { }

    /**
     * getter for id
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     * setter for id
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * getter for userId
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     * setter for username
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * getter for userId
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     * setter for password
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * getter for status
     * @return
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * setter for status
     * @param status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * getter for salt
     * @return
     */
    public String getSalt() {
        return salt;
    }

    /**
     * setter for salt
     * @param salt
     */
    public void setSalt(String salt) {
        this.salt = salt;
    }

    /**
     * getter for gmtLastLogin
     * @return
     */
    public Date getGmtLastLogin() {
        return gmtLastLogin;
    }

    /**
     * setter for gmtLastLogin
     * @param gmtLastLogin
     */
    public void setGmtLastLogin(Date gmtLastLogin) {
        this.gmtLastLogin = gmtLastLogin;
    }

    /**
     * to string
     * @return
     */
    @Override
    public String toString() {
        return "IdsUserEntity{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", salt='" + salt + '\'' +
                ", password='" + password + '\'' +
                ", status=" + status +
                ", gmtLastLogin=" + gmtLastLogin +
                '}';
    }

}
